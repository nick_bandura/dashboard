import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { environment } from '../environments/environment';

import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import {DashboardModule} from './components/dashboard/dashboard.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// function initializeKeycloak(keycloak: KeycloakService): () => any {
//   return () =>
//     keycloak.init({
//       config: {
//         realm: environment.keycloakRealm,
//         clientId: environment.keycloakClientID,
//       }
//     });
// }

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    KeycloakAngularModule,
    DashboardModule,
    BrowserAnimationsModule
  ],
  providers: [
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: initializeKeycloak,
    //   multi: true,
    //   deps: [KeycloakService],
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
