import {Component, OnInit} from '@angular/core';
import {DashboardService} from './dashboard.service';
import {MatDialog} from '@angular/material/dialog';
import {EditDialogComponent} from './edit-dialog/edit-dialog.component';
import {editableTypes} from './dashboard.constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public devices;
  public headers;
  public editableTypes = editableTypes;

  constructor(private dashboardService: DashboardService, private dialog: MatDialog) {}

  async ngOnInit(): Promise<void> {
    const {devices, headers} = await this.dashboardService.getDevices();
    this.devices = devices;
    this.headers = headers;

    console.log(devices, headers);
  }

  editEntry(device, field) {
    if (!editableTypes.includes(field)) return;

    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: device[field],
      minWidth: '500px',
      maxWidth: '50vw'
    });

    dialogRef.afterClosed().subscribe(newEntry => {
      if (newEntry) {
        const updatedDevice = {...device};
        updatedDevice[field] = newEntry;

        this.dashboardService.updateDevice(updatedDevice)
          .subscribe(newDevice => {
            newDevice[field] = JSON.stringify(newDevice[field]);
            device = newDevice;
          });

        device[field] = JSON.stringify(newEntry);
      }
    });
  }
}
