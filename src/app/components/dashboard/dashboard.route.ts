import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
// import {KeycloakGuard} from '../../auth/keycloak.guard';

export const routes: Route[] = [{
  component: DashboardComponent,
  path: ''
  // canActivate: [KeycloakGuard]
}];
