import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) {}

  getDevices(): Promise<any> {
    return this.http.get('/devices').pipe(map((devices: any) => {
      const headers = Object.keys(devices.reduce((value, el) => ({...value, ...el}), {}));

      devices.forEach(device => {
        for (const key in device) {
          if (typeof device[key] === 'object') {
            device[key] = JSON.stringify(device[key]);
          }
        }
      });

      return {headers, devices};
    })).toPromise();
  }

  updateDevice(entry): Observable<any> {
    return this.http.put(`devices/${entry._id}`, entry, {observe: 'body'});
  }
}
