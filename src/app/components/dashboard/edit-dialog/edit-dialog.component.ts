import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent {
  public fields;

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {
    console.log(this.data);
    this.fields = JSON.parse(this.data);
  }

  trackByFn(index, item) {
    return item.key;
  }
}
