import {Component, Input, OnInit, Output} from '@angular/core';
import {EventEmitter} from '@angular/core';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {
  @Input() field;
  @Input() key;
  @Input() order;
  @Output() fieldChange = new EventEmitter();
  public isObject: boolean;
  public isBoolean: boolean;
  public isNumber: boolean;
  public isString: boolean;
  public inputValue;

  constructor() {}

  ngOnInit(): void {
    this.isObject = (typeof this.field === 'object') && !Array.isArray(this.field);
    this.isBoolean = typeof this.field === 'boolean';
    this.isNumber = typeof this.field === 'number';
    this.isString = typeof this.field === 'string';
  }

  onChange(value) {
    this.fieldChange.emit(value);
  }

  onPublicChange(value) {
    this.field.public = value;
  }

  trackByFn(index, item) {
    return item.key;
  }
}
