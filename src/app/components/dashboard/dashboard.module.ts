import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {RouterModule} from '@angular/router';
import {routes} from './dashboard.route';
import {DashboardService} from './dashboard.service';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { InputFieldComponent } from './edit-dialog/input-field/input-field.component';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {SearchModule} from '../search/search.module';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [
    DashboardComponent,
    EditDialogComponent,
    InputFieldComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    MatFormFieldModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    FormsModule,
    SearchModule,
    MatButtonModule
  ],
  providers: [
    DashboardService
  ]
})
export class DashboardModule { }
