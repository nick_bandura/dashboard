import { Component, OnInit } from '@angular/core';
import {SearchService} from './search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public types;
  public searchText: string;
  public selectedTypes;

  constructor(private searchService: SearchService) {}

  async ngOnInit(): Promise<void> {
    this.types = await this.searchService.getDeviceTypes();
  }

  search() {
    console.log(this.selectedTypes, this.searchText);
    // this.searchService.search(this.selectedTypes);
  }
}
