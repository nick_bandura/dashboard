import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() { }

  getDeviceTypes() {
    return [
      {
        name: 'Electrometer',
        pk_entity_type_id: '12'
      },
      {
        name: 'CO2 Sensor',
        pk_entity_type_id: '525'
      },
      {
        name: 'Movement sensor',
        pk_entity_type_id: '42'
      },
      {
        name: 'Smoke detector',
        pk_entity_type_id: '13'
      },
      {
        name: 'CCTV Camera',
        pk_entity_type_id: '0'
      }
    ];
  }
}
