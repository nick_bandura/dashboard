export const environment = {
  production: false,
  keycloakURL: 'http://localhost:8080/auth',
  keycloakRealm: 'dashboard',
  keycloakClientID: 'account'
};
