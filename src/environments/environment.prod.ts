export const environment = {
  production: true,
  keycloakURL: 'http://localhost:8080/auth',
  keycloakRealm: 'dashboard',
  keycloakClientID: 'account'
};
